#!/usr/bin/env python2
from spnav import *
from pymouse import PyMouse
from time import sleep
import math

def motion_event(event, xpos, ypos, down, lastScroll, m):
    scale = 20.0
    scrollScale = 1
    scrollN = 4
    x, z, y = event.translation
    rx, ry, rz = event.rotation
    xmove = 0;
    if abs(x) > 10:
        xmove = x/scale
    ymove = 0;
    if abs(y) > 10:
        ymove = -y/scale
    xpos += xmove
    ypos += ymove
    m.move(xpos, ypos)

    if abs(rx) > 200:
        if lastScroll == 0:
            m.scroll(vertical = -math.copysign(scrollScale, rx))
            lastScroll = scrollN
        else:
            lastScroll = lastScroll -1
    else:
        lastScroll = 0
    if z < -200:
        if not down:
            m.click(xpos, ypos)
        down = True
    else:
        down = False
    return (xpos, ypos, down, lastScroll)

def button_event(event, xpos, ypos, m):
    button = 1
    if event.bnum == 1:
        button = 2
    if event.press:
        m.press(xpos, ypos, button) 
    else:
        m.release(xpos, ypos, button) 

if __name__ == "__main__":
    spnav_open()
    m = PyMouse()
    event = None
    xpos = 0
    ypos = 0
    down = False
    lastScroll = False
    try:
        while True:
            while event == None:
                event = spnav_poll_event()
            if event.ev_type == SPNAV_EVENT_MOTION:
                xpos, ypos, down, lastScroll = motion_event(event, xpos, ypos, down, lastScroll, m)
            if event.ev_type == SPNAV_EVENT_BUTTON:
                button_event(event, xpos, ypos, m)
            event = None
    finally:
        spnav_close()
